##Usage of Struct in Java
**To read the story**: 

**Estimated reading time**: 20-25m

## Story Outline
In programming, the struct is a keyword for creating a structure that contains variables, methods, different types of constructors, operators, etc. It is similar to classes that hold different types of data and has a value type. It creates objects which require less memory.

However, structs are not present in Java. We can modify some objects in Java to use them as a struct
## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: #method-struct-java, #java, #struct-in-java
